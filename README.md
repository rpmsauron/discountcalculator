primeit
=======

A Symfony project created on March 22, 2018, 0:12 am.


Intro
=====

The project was developed under Ubuntu 16.04, with Symfony, and it is a take on the technical task proposed at:
`https://github.com/teamleadercrm/coding-test/blob/master/1-discounts.md`

The files provided are also available from copying, inside the directory:
```
./TECHNICAL_TASK/
```

Interpretation of the domain from the description of the requirements:

1. A Customer may have various Orders;

1. An Order has a set of various bundles of Products;

1. A Product may be in several Orders;

1. This many-to-many relation between Products and Orders manifest on a intermediary database table and Entity on the project, udner the name of Productpack, which represents a certain amount of a certain product within an Order.

A few assumptions were made from reading the project:
Since a standard API would, by definition, provide instead a route to get the Discounts under a certain pattern such as providing an id to the route to get the Order instead of input POST data (providing a full set of information pertaining to the Order and its related entities), I found it relevant that only Orders that actually exist on the database by matching the given input data would be returned. This means that if any inexistent or unmatching information is sent on the Request to get the Discounts, it will result in a "400 Error Bad request" as expected from any API.
This is what I believe to be a flaw in the way the fetching of the information was requested.
It should have been instead something like:
`GET: http://mylocalhostname/api/order/{id}/discounts`

Setup
=====

1. Install a webserver (Apache is advised which the development of the project was built upon), with PHP 7.1 at least, and mysql.

1. Clone the Git repository from:
`git@bitbucket.org:rpmsauron/primeit.git`

1. Change directory into the root of the project, and, to fetch vendor folder to get third party resources for Symfony project, run:
`composer install`

1. Use a REST client like Psotman or Insomnia to test REST Requests of the API.
Insomnia is adived since settings and history of Requests for the project are provided inside:
`./test_requests/Insomnia_2018-03 Primeit API requests.json`
which can be imported into Insomnia.

1. You can import the database data I tested the API with during development, from:
`mysql -uroot -pkarolina primeit < database/tests.sql`

1. The database connection is by default configured to a database named "primeit", user as "root" and password as "karolina".
Adapt the connection values to your own local password to test it, by changing them in:
`app/config/parameters.yml`

You can access the documentation of the API at:
`http://<localhost>/api/doc`

Any doubts or questions feel free to contact me at:
`rpmsauron[NO_SPAM] AT gmail COM com`

Rui M Silva, 2018
