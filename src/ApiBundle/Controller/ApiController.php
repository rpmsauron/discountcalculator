<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Category;
use ApiBundle\Entity\Customer;
use ApiBundle\Entity\Order;
use ApiBundle\Entity\Product;
use ApiBundle\Helper\CategoryHelper;
use ApiBundle\Helper\CustomerHelper;
use ApiBundle\Helper\OrderHelper;
use ApiBundle\Helper\ProductHelper;
use ApiBundle\Repository\CategoryRepository;
use ApiBundle\Repository\CustomerRepository;
use ApiBundle\Repository\OrderRepository;
use ApiBundle\Repository\ProductRepository;
use ApiBundle\Service\DiscountService;
use FOS\RestBundle\Controller\Annotations as HTTPAnnotations;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Operation;

class ApiController extends FOSRestController
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var CategoryHelper
     */
    private $categoryHelper;

    /**
     * @var CustomerHelper
     */
    private $customerHelper;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var DiscountService
     */
    private $discountService;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var ProductHelper
     */
    private $productHelper;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Creates a new AppController.
     *
     * @parem CategoryHelper        $categoryHelper        The Category Helper.
     * @parem CategoryRepository    $categoryRepository    The Category Repository.
     * @parem CustomerHelper        $customerHelper        The Customer Helper.
     * @parem CustomerRepository    $customerRepository    The Customer Repository.
     * @param DiscountService       $discountService       The Discount Service.
     * @param OrderHelper           $orderHelper           The Order Helper.
     * @parem OrderRepository       $orderRepository       The Order Repository.
     * @param ProductHelper         $productHelper         The Product Helper.
     * @parem ProductRepository     $productRepository     The Product Repository.
     */
    public function __construct(
        CategoryHelper $categoryHelper,
        CategoryRepository $categoryRepository,
        CustomerHelper $customerHelper,
        CustomerRepository $customerRepository,
        DiscountService $discountService,
        OrderHelper $orderHelper,
        OrderRepository $orderRepository,
        ProductHelper $productHelper,
        ProductRepository $productRepository
    ) {
        $this->categoryHelper        = $categoryHelper;
        $this->categoryRepository    = $categoryRepository;
        $this->customerHelper        = $customerHelper;
        $this->customerRepository    = $customerRepository;
        $this->discountService       = $discountService;
        $this->orderHelper           = $orderHelper;
        $this->orderRepository       = $orderRepository;
        $this->productHelper         = $productHelper;
        $this->productRepository     = $productRepository;
    }

    /**
     * Creates a Customer.
     *
     * ### Request Example
     *     {
     *         "name": "George Orwell",
     *         "since": "2015-06-21",
     *         "revenue": "1000.50"
     *     }
     * ###
     *
     * @HTTPAnnotations\Post("/customer/new")
     * @Operation(
     *     summary="Creates a Customer",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Request $request The HTTP Request.
     *
     * @return The HTTP Response.
     */
    public function postCustomerAction(Request $request): Response
    {
        $postData = json_decode($request->getContent(), true);
        $this->customerHelper->createCustomer($postData);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Removes a Customer.
     *
     * ### Request Example ###
     *
     * @HTTPAnnotations\Post("/customer/delete/{id}")
     * @Operation(
     *     summary="Removes a Customer",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Customer $customer The Customer to delete.
     *
     * @return The HTTP Response.
     */
    public function deleteCustomerAction(Customer $category): Response
    {
        $this->customerRepository->delete($category);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Creates a Category.
     *
     * ### Request Example
     *     {
     *         "name": "kitchenware"
     *     }
     * ###
     *
     * @HTTPAnnotations\Post("/category/new")
     * @Operation(
     *     summary="Creates a Category",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Request $request The HTTP Request.
     *
     * @return The HTTP Response.
     */
    public function postCategoryAction(Request $request): Response
    {
        $postData = json_decode($request->getContent(), true);
        $this->categoryHelper->createCategory($postData);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Removes a Category.
     *
     * ### Request Example ###
     *
     * @HTTPAnnotations\Post("/category/delete/{id}")
     * @Operation(
     *     summary="Removes a Customer",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Category $category The Category to delete.
     *
     * @return The HTTP Response.
     */
    public function deleteCategoryAction(Category $category): Response
    {
        $this->categoryRepository->delete($category);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Creates a Product.
     *
     * ### Request Example
     *     {
     *         "description": "Forks Silverware xtrade",
     *         "price": "155",
     *         "category": "14"
     *     }
     * ###
     *
     * @HTTPAnnotations\Post("/product/new")
     * @Operation(
     *     summary="Creates a Product",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Request $request The HTTP Request.
     *
     * @return The HTTP Response.
     */
    public function postProductAction(Request $request): Response
    {
        $postData = json_decode($request->getContent(), true);
        $this->productHelper->createProduct($postData);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Removes a Product.
     *
     * ### Request Example ###
     *
     * @HTTPAnnotations\Post("/product/delete/{id}")
     * @Operation(
     *     summary="Creates a Product",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Product $product The Product to delete.
     *
     * @return The HTTP Response.
     */
    public function deleteProductAction(Product $product): Response
    {
        $this->productRepository->delete($product);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Creates an Order.
     *
     * ### Request Example ###
     *     {
     *         "customer-id": "4",
     *         "items": [
     *             {
     *                 "product-id": "7",
     *                 "quantity": "10"
     *             },
     *             {
     *                 "product-id": "18",
     *                 "quantity": "2"
     *             },
     *             {
     *                 "product-id": "17",
     *                 "quantity": "14"
     *             }
     *         ]
     *     }
     * ###
     *
     * @HTTPAnnotations\Post("/order/new")
     * @Operation(
     *     summary="Creates an Order",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Request $request The HTTP Request.
     *
     * @return The HTTP Response.
     */
    public function postOrderAction(Request $request): Response
    {
        $postData = json_decode($request->getContent(), true);
        $this->orderHelper->createOrder($postData);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Removes an Order.
     *
     * ### Request Example ###
     *
     * @HTTPAnnotations\Post("/order/delete/{id}")
     * @Operation(
     *     summary="Creates an Order",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Schema(
     *             example="ok"
     *         )
     *     )
     * )
     *
     * @param  Order $order The Order to delete.
     *
     * @return The HTTP Response.
     */
    public function deleteOrderAction(Order $order): Response
    {
        $this->orderRepository->delete($order);

        return new Response(json_encode("ok"), Response::HTTP_OK);
    }

    /**
     * Gets an Order.
     *
     * ### Request Example ###
     *
     * @HTTPAnnotations\Get("/order/{id}")
     * @Operation(
     *     summary="Creates an Order",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Property(
     *             type="json",
     *             example={
     *                 "id": "4",
     *                 "customer-id": "3",
     *                 "items": {
     *                     {
     *                         "product-id": "5",
     *                         "quantity": "30",
     *                         "unit-price": "4.99",
     *                         "total": "149.7"
     *                     },
     *                 },
     *                 "total": "149.70"
     *             }
     *         )
     *     )
     * )
     *
     * @param  Order $order The Order to get.
     *
     * @return The HTTP Response.
     */
    public function getOrderAction(Order $order): Response
    {
        $response = new Response();
        $content  = $order->normalize();
        $response->setContent($content);
        $response->headers->set("Content-Type", "application/json");
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }

    /**
     * Gets the Discounts applicable for an Order.
     *
     * ### Request Example
     *     {
     *         "id": "1",
     *         "customer-id": "3",
     *         "items": [
     *             {
     *                 "product-id": "4",
     *                 "quantity": "10",
     *                 "unit-price": "4.99",
     *                 "total": "49.9"
     *             },
     *             {
     *                 "product-id": "5",
     *                 "quantity": "10",
     *                 "unit-price": "4.99",
     *                 "total": "49.9"
     *             },
     *         ],
     *         "total": "99.80"
     *     }
     * ###
     *
     * @HTTPAnnotations\Post("/order/discount")
     * @Operation(
     *     summary="Gets the Discounts applicable for an Order",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Property(
     *             type="json",
     *             example={
     *                 {
     *                     "discount": "DISCOUNT_1",
     *                     "description": "10% Discount on Order for spending over 1000€.",
     *                     "details": "Order was 99.8€ and now is 89.82€."
     *                 },
     *                 {
     *                     "discount": "DISCOUNT_2",
     *                     "description": "For category 'switches', you get a free product for each 5 you buy.",
     *                     "details": "You got 2 for free on Product id '4'."
     *                 },
     *                 {
     *                     "discount": "DISCOUNT_2",
     *                     "description": "For category 'switches', you get a free product for each 5 you buy.",
     *                     "details": "You got 2 for free on Product id '5'."
     *                 }
     *             }
     *         )
     *     )
     * )
     *
     * @param  Request $request The HTTP Request.
     *
     * @return The HTTP Response.
     */
    public function postDiscountAction(Request $request): Response
    {
        $postData = json_decode($request->getContent(), true);
        $order = $this->orderHelper->toOrder($postData);
        $discounts = $this->discountService->getDiscounts($order);
        
        $response = new Response();
        $response->setContent(json_encode($discounts));
        $response->headers->set("Content-Type", "application/json");
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }
}
