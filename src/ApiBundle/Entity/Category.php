<?php

namespace ApiBundle\Entity;

use ApiBundle\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="category")
 */
class Category
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Product", mappedBy="category")
     */
    private $products;

    /**
     * Creates a new Category.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * Gets the id.
     *
     * @return int The id of the Category.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the name.
     *
     * @return string The name of the Category.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets the id.
     *
     * @param int $id The id of the Category.
     *
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Sets the name.
     *
     * @param string $name The name of the Category.
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Gets the Products.
     *
     * @return Product[] The Products.
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Sets the Products.
     *
     * @param  Product[] $products The Products of this Category.
     *
     * @return void
     */
    public function setProducts(ArrayCollection $products): void
    {
        $this->products = $products;
    }

    /**
     * Adds Product to this Category.
     *
     * @param  Product $product The Product to add to this Category.
     *
     * @return void
     */
    public function addProduct(Product $product): void
    {
        $this->products->add($product);
    }

    /**
     * Removes Product from this Category.
     *
     * @param  Product $product The Product to remove from this Category.
     *
     * @return void
     */
    public function removeProduct(Product $product): void
    {
        $this->products->removeElement($product);
    }
}
