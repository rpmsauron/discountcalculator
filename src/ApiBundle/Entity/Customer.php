<?php

namespace ApiBundle\Entity;

use ApiBundle\Entity\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\CustomerRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="customer")
 */
class Customer
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="since", type="datetime", nullable=false)
     */
    private $since;

    /**
     * @ORM\Column(name="revenue", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $revenue;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Order", mappedBy="customer")
     */
    private $orders;

    /**
     * Creates a new Customer.
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    /**
     * Gets the id;
     *
     * @return int The id of the Customer.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the name.
     *
     * @return string The name of the Customer.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Gets the "since" date value.
     *
     * @return \DateTime The "since" date value of the Customer.
     */
    public function getSince(): \DateTime
    {
        return $this->since;
    }

    /**
     * Gets the revenue.
     *
     * @return float The revenue.
     */
    public function getRevenue(): float
    {
        return $this->revenue;
    }

    /**
     * Gets the Orders.
     *
     * @return array The Orders.
     */
    public function getOrders()
    {
        return $this->orders;
    }

    
    /**
     * Sets the id.
     *
     * @param  int $id The id.
     *
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Sets the name.
     *
     * @param  string $name The name of the Customer.
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Sets the "since" date value.
     *
     * @param  \DateTime $since The "since" date value of the Customer.
     *
     * @return void
     */
    public function setSince(\DateTime $since): void
    {
        $this->since = $since;
    }

    /**
     * Sets the revenue.
     *
     * @param  float $revenue The revenue of the Customer.
     *
     * @return void
     */
    public function setRevenue(float $revenue): void
    {
        $this->revenue = $revenue;
    }

    
    /**
     * Sets the Orders.
     *
     * @param  Order[] $orders The Orders of this Customer.
     *
     * @return void
     */
    public function setOrders(ArrayCollection $orders): void
    {
        $this->orders = $orders;
    }

    /**
     * Adds Order to this Customer.
     *
     * @param  Order $order The Order to add to this Customer.
     *
     * @return void
     */
    public function addOrder(Order $order): void
    {
        $this->orders->add($order);
    }

    /**
     * Removes Order from this Customer.
     *
     * @param  Order $order The Order to remove from this Customer.
     *
     * @return void
     */
    public function removeOrder(Order $order): void
    {
        $this->orders->removeElement($order);
    }

    /**
     * Returns the total spent by this Customer on every Order.
     *
     * @return float The total spent by this Customer on every Order.
     */
    public function getTotalSpent(): float
    {
        $total = 0;
        foreach ($this->orders as $order) {
            $total += $order->getTotal();
        }

        return $total;
    }
}
