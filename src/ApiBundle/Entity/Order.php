<?php

namespace ApiBundle\Entity;

use ApiBundle\Entity\Customer;
use ApiBundle\Entity\Productpack;
use ApiBundle\Normalizer\NormalizedOrder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="ApiBundle\Entity\Customer",
     *     inversedBy="orders"
     * )
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Productpack", mappedBy="order")
     */
    private $productpacks;

    /**
     * Creates a new Order.
     */
    public function __construct()
    {
        $this->productpacks = new ArrayCollection();
    }

    /**
     * Gets the id.
     *
     * @return int The id.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the Customer.
     *
     * @return Customer The Customer.
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * Gets the Product Packs.
     *
     * @return Productpack[] The Product Packs of this Order.
     */
    public function getProductpacks(): array
    {
        return $this->productpacks;
    }

    /**
     * Sets the id.
     *
     * @param  int $id The id of the Order.
     *
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Sets the Customer.
     *
     * @param  Customer $customer The Customer of the Order.
     *
     * @return void
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }


    /**
     * Sets the Productpacks of this Order.
     *
     * @param  Productpack[] $productpacks The Productpacks.
     *
     * @return void
     */
    public function setProductpacks(ArrayCollection $productpacks): void
    {
        $this->productpacks = $productpacks;
    }

    /**
     * Adds Productpack to this Order.
     *
     * @param  Productpack $productpack The Productpack to add to this Order.
     *
     * @return void
     */
    public function addProductpack(Productpack $productpack): void
    {
        $this->productpacks->add($productpack);
    }

    /**
     * Removes Productpack from this Order.
     *
     * @param  Productpack $productpack The Productpack to remove from this Order.
     *
     * @return void
     */
    public function removeProductpack(Productpack $productpack): void
    {
        $this->productpacks->removeElement($productpack);
    }

    /**
     * Returns a normalized json encoding of this Order object.
     *
     * @return string The Normalized Order.
     */
    public function normalize(): string
    {
        $normalizedOrder = new NormalizedOrder($this);
        $encodedOrder = json_encode($normalizedOrder);
        $encodedOrder = str_replace("customer_id", "customer-id", $encodedOrder);
        $encodedOrder = str_replace("product_id", "product-id", $encodedOrder);
        $encodedOrder = str_replace("unit_price", "unit-price", $encodedOrder);

        return $encodedOrder;
    }

    /**
     * Returns the total of this Order.
     *
     * @return float The total of this Order.
     */
    public function getTotal(): float
    {
        $total = 0;
        foreach ($this->getProductpacks() as $productpack) {
            $total += ($productpack->getQuantity() * $productpack->getProduct()->getPrice());
        }

        return $total;
    }
}
