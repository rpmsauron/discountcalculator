<?php

namespace ApiBundle\Entity;

use ApiBundle\Entity\Productpack;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     */
    private $description;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="ApiBundle\Entity\Category",
     *     inversedBy="products"
     * )
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Productpack", mappedBy="product")
     */
    private $productpacks;

    /**
     * Creates a new Product.
     */
    public function __construct()
    {
        $this->productpacks = new ArrayCollection();
    }

    /**
     * Gets the id.
     *
     * @return int The id of the Product.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the description.
     *
     * @return string The description of the Product.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * gets the Category.
     *
     * @return Category The Category of the Product.
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * Gets the price.
     *
     * @return float The price of the Product.
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Sets the id.
     *
     * @param  int $id The id of the Product.
     *
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Sets the description.
     *
     * @param  string $description The description of the Product.
     *
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * Sets the Category.
     *
     * @param  Category $category The Category of the Product.
     *
     * @return void
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /**
     * Sets the price.
     *
     * @param  float $price The price of the Product.
     *
     * @return void
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * Sets the Productpacks of this Product.
     *
     * @param  Productpack[] $productpacks The Productpacks.
     *
     * @return void
     */
    public function setProductpacks(ArrayCollection $productpacks): void
    {
        $this->productpacks = $productpacks;
    }

    /**
     * Adds a Productpack to this Product.
     *
     * @param  Productpack $productpack The Productpack to add to this Product.
     *
     * @return void
     */
    public function addProductpack(Productpack $productpack): void
    {
        $this->productpacks->add($productpack);
    }

    /**
     * Removes a Productpack from this Product.
     *
     * @param  Productpack $productpack The Productpack to remove from this Product.
     *
     * @return void
     */
    public function removeProductpack(Productpack $productpack): void
    {
        $this->productpacks->removeElement($productpack);
    }
}
