<?php

namespace ApiBundle\Entity;

use ApiBundle\Entity\Order;
use ApiBundle\Entity\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ProductpackRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="productpack")
 */
class Productpack
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="quantity", type="integer", length=10, nullable=false)
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="ApiBundle\Entity\Product",
     *     inversedBy="productpacks"
     * )
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $product;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="ApiBundle\Entity\Order",
     *     inversedBy="productpacks"
     * )
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $order;

    /**
     * Gets the id.
     *
     * @return int The id of the Productpack.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Gets the quantity.
     *
     * @return int The quantity of the Productpack.
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Gets the Product.
     *
     * @return Product The Product of the Productpack.
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * Gets the Order.
     *
     * @return Order The Order of the Productpack.
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * Sets the id.
     *
     * @param  int $id The id of the Productpack.
     *
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Sets the quantity.
     *
     * @param  int $quantity The quantity of the Productpack.
     *
     * @return void
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * Sets the Product.
     *
     * @param  Product $product The Product of the Productpack.
     *
     * @return void
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * Sets the Order.
     *
     * @param  Order $order The Order of the Productpack.
     *
     * @return void
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    /**
     * Returns the total of this Productpack.
     *
     * @return float The total of this Productpack.
     */
    public function getTotal(): float
    {
        return $this->quantity * $this->product->getPrice();
    }
}
