<?php

namespace ApiBundle\Helper;

use ApiBundle\Entity\Category;
use ApiBundle\Repository\CategoryRepository;

class CategoryHelper
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * Creates a new Product Helper.
     *
     * @param CategoryRepository $categoryRepository The Category Repository.
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Creates a Category from an array of data.
     *
     * @param  array    $postData The array of data from which to create the Category.
     *
     * @return Category The Category created.
     */
    public function createCategory(array $postData): Category
    {
        $name     = $postData["name"];
        $category = new Category();
        $category->setName($name);
        $this->categoryRepository->save($category);

        return $category;
    }
}
