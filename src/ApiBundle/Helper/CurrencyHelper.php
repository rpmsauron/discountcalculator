<?php

namespace ApiBundle\Helper;

class CurrencyHelper
{
    /**
     * Validates number is in currency format, i.e.: 1550.50, 1500 or 100.75, etc.
     *
     * Regex:
     *
     * \b      # word boundary assertion
     * \d{1,3} # 1-3 digits
     * (?:     # followed by this group...
     *  ,?     # an optional comma
     *  \d{3}  # exactly three digits
     * )*      # ...any number of times
     * (?:     # followed by this group...
     *  \.     # a literal dot
     *  \d{2}  # exactly two digits
     * )?      # ...zero or one times
     * \b      # word boundary assertion
     *
     * @param  mixed $number The number to verify.
     *
     * @return bool  True if is in Currency format, false otherwise.
     */
    public function isCurrency($number): bool
    {
        return preg_match("/\b\d{1,3}(?:,?\d{3})*(?:\.\d{2})?\b/", $number);
    }
}
