<?php

namespace ApiBundle\Helper;

use ApiBundle\Entity\Customer;
use ApiBundle\Repository\CustomerRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CustomerHelper
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * Creates a new Product Helper.
     *
     * @param CustomerRepository $customerRepository The Customer Repository.
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Creates a Customer from an array of data.
     *
     * @param  array    $postData The array of data from which to create the Customer.
     *
     * @return Customer The Customer created.
     */
    public function createCustomer(array $postData): Customer
    {
        $format  = "Y-m-d";
        $name    = $postData["name"];
        $since   = $postData["since"];
        $revenue = $postData["revenue"];
        $sinceDatetime = \DateTime::createFromFormat($format, $since);
        $now = new \DateTime();

        if ($now < $sinceDatetime) {
            throw new BadRequestHttpException(
                "Bad request: since value is invalid; it needs to be a date from the past."
            );
        }

        $customer = new Customer();
        $customer->setName($name);
        $customer->setRevenue($revenue);
        $customer->setSince($sinceDatetime);
        $this->customerRepository->save($customer);

        return $customer;
    }
}
