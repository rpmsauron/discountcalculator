<?php

namespace ApiBundle\Helper;

use ApiBundle\Entity\Order;
use ApiBundle\Entity\Productpack;
use ApiBundle\Repository\CustomerRepository;
use ApiBundle\Repository\OrderRepository;
use ApiBundle\Repository\ProductpackRepository;
use ApiBundle\Repository\ProductRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrderHelper
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ProductpackRepository
     */
    private $productpackRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * Creates a new Order Helper.
     *
     * @param CustomerRepository    $customerRepository    The Customer Repository.
     * @param OrderRepository       $orderRepository       The Order Repository.
     * @param ProductpackRepository $productpackRepository The Productpack Repository.
     * @param ProductRepository     $productRepository     The Product Repository.
     */
    public function __construct(
        CustomerRepository $customerRepository,
        OrderRepository $orderRepository,
        ProductpackRepository $productpackRepository,
        ProductRepository $productRepository
    ) {
        $this->customerRepository    = $customerRepository;
        $this->orderRepository       = $orderRepository;
        $this->productpackRepository = $productpackRepository;
        $this->productRepository     = $productRepository;
    }

    /**
     * Creates an Order and respective Productpacks from an array of data.
     *
     * @param  array $postData The array of data from which to create the Order.
     *
     * @return Order The Order created.
     */
    public function createOrder(array $postData): Order
    {
        $customerId   = $postData["customer-id"];
        $productsJson = $postData["items"];
        $customer = $this->customerRepository->getById($customerId);

        $order = new Order();
        $order->setCustomer($customer);

        // Premature save of Entity so it can be related to other entities further, but before flush.
        $this->orderRepository->save($order, true);

        foreach ($productsJson as $productJson) {
            $quantity = $productJson["quantity"];
            $product  = $this->productRepository->getById((int)$productJson["product-id"]);

            $productpack = new Productpack();
            $productpack->setOrder($order);
            $productpack->setProduct($product);
            $productpack->setQuantity($quantity);

            $this->productpackRepository->save($productpack, true);

            $order->addProductpack($productpack);
        }

        $this->orderRepository->flush();

        return $order;
    }

    /**
     * Matches a json structure to an existing Order.
     *
     * @param  array $postData The json structure representing an Order.
     *
     * @return Order The Order to match the json structure to.
     *
     * @throws BadRequestHttpException
     */
    public function toOrder(array $postData): Order
    {
        $id           = $postData["id"];
        $customerId   = $postData["customer-id"];
        $productsJson = $postData["items"];
        $orderTotal   = $postData["total"];
        $order = $this->orderRepository->getById($id);

        if ($order->getCustomer()->getId() != $customerId) {
            throw new BadRequestHttpException("Bad request: order with such data does not exist: invalid customer id");
        }

        foreach ($productsJson as $productJson) {
            $productId = (int) $productJson["product-id"];
            $quantity = $productJson["quantity"];
            $price = $productJson["unit-price"];
            $productpack  = $this->productpackRepository->findOneBy(
                [
                    "product" => $productId,
                    "order" => $order->getId()
                ]
            );
            $productTotal = $productJson["total"];

            if ($productpack && (
                $productpack->getProduct()->getId() != $productId
                    || $productpack->getQuantity() != $quantity
                    || $productpack->getProduct()->getPrice() != $price
                    || $this->isTotalEqual($productpack->getTotal(), $productTotal)
                )
            ) {
                throw new BadRequestHttpException(
                    "Bad request: order with such data does not exist: invalid product id,"
                        . "quantity or price or total."
                );
            }
        }

        if ($this->isTotalEqual($order->getTotal(), $orderTotal)) {
            throw new BadRequestHttpException(
                "Bad request: order with such data does not exist: invalid order total"
            );
        }

        return $order;
    }

    /**
     * Compares the values of two float totals by precision decimals of two.
     *
     * @param  float $total1 The total1 value to compare.
     * @param  float $total2 The total2 value to compare.
     *
     * @return bool  Returns true if they are equal, false otherwise.
     */
    private function isTotalEqual(float $total1, float $total2): bool
    {
        return number_format((float) $total1, 2, ".", "") != number_format((float) $total2, 2, ".", "");
    }
}
