<?php

namespace ApiBundle\Helper;

use ApiBundle\Entity\Product;
use ApiBundle\Helper\CurrencyHelper;
use ApiBundle\Repository\CategoryRepository;
use ApiBundle\Repository\ProductRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductHelper
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var CurrencyHelper
     */
    private $currencyHelper;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Creates a new Product Helper.
     *
     * @param CategoryRepository $categoryRepository The Category Repository.
     * @param CurrencyHelper     $currencyHelper     The Currency Helper.
     * @param ProductRepository  $productRepository  The Product Repository.
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        CurrencyHelper $currencyHelper,
        ProductRepository $productRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->currencyHelper     = $currencyHelper;
        $this->productRepository  = $productRepository;
    }

    /**
     * Creates a Product from an array of data.
     *
     * @param  array   $postData The array of data from which to create the Product.
     *
     * @return Product The Product created.
     */
    public function createProduct(array $postData): Product
    {
        $categoryId  = $postData["category"];
        $description = $postData["description"];
        $price       = $postData["price"];
        $category    = $this->categoryRepository->getById($categoryId);

        if (!$this->currencyHelper->isCurrency($price)) {
            throw new BadRequestHttpException(
                "Bad request: price is not in Currency format."
            );
        }

        $product = new Product();
        $product->setDescription($description);
        $product->setCategory($category);
        $product->setPrice($price);
        $this->productRepository->save($product);

        return $product;
    }
}
