<?php

namespace ApiBundle\Normalizer;

use ApiBundle\Entity\Order;

class NormalizedOrder
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $customer_id;

    /**
     * @var array
     */
    public $items;

    /**
     * @var string
     */
    public $total;

    /**
     * Creates a new Normalized Order.
     *
     * @param Order $order The Order to normalize.
     */
    public function __construct(Order $order)
    {
        $items      = [];
        $orderTotal = 0;

        foreach ($order->getProductpacks() as $productpack) {
            $itemsTotal = $productpack->getQuantity() * $productpack->getProduct()->getPrice();
            $items[] = [
                "product_id" => (string) $productpack->getProduct()->getId(),
                "quantity"   => (string) $productpack->getQuantity(),
                "unit_price" => (string) $productpack->getProduct()->getPrice(),
                "total"      => (string) $itemsTotal
            ];
            $orderTotal += $itemsTotal;
        }

        $this->id          = (string) $order->getId();
        $this->customer_id = (string) $order->getCustomer()->getId();
        $this->items       = $items;
        $this->total       = number_format((float) $orderTotal, 2, ".", "");
    }
}
