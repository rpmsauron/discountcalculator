<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Category;
use ApiBundle\Repository\RepositoryTrait;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Category Repository.
 */
class CategoryRepository extends EntityRepository
{
    use RepositoryTrait;

    /**
     * Gets a Category by its id.
     *
     * @param  int      $id The id to get Category for.
     *
     * @return Category The Category matching the id.
     *
     * @throws BadRequestHttpException
     */
    public function getById(int $id): Category
    {
        $category = parent::find($id);
        if (!$category instanceof Category) {
            throw new BadRequestHttpException(
                "Bad request: category does not exist."
            );
        }

        return $category;
    }
}
