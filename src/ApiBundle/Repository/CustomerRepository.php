<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Customer;
use ApiBundle\Repository\RepositoryTrait;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Customer Repository.
 */
class CustomerRepository extends EntityRepository
{
    use RepositoryTrait;

    /**
     * Gets a Customer by its id.
     *
     * @param  int      $id The id to get Customer for.
     *
     * @return Customer The Customer matching the id.
     *
     * @throws BadRequestHttpException
     */
    public function getById(int $id): Customer
    {
        $customer = parent::find($id);
        if (!$customer instanceof Customer) {
            throw new BadRequestHttpException(
                "Bad request: customer does not exist."
            );
        }

        return $customer;
    }
}
