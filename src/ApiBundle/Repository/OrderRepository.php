<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Order;
use ApiBundle\Repository\RepositoryTrait;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Order Repository.
 */
class OrderRepository extends EntityRepository
{
    use RepositoryTrait;

    /**
     * Gets an Order by its id.
     *
     * @param  int $id The id to get Order for.
     *
     * @return Order The Order matching the id.
     *
     * @throws BadRequestHttpException
     */
    public function getById(int $id): Order
    {
        $order = parent::find($id);
        if (!$order instanceof Order) {
            throw new BadRequestHttpException(
                "Bad request: order does not exist."
            );
        }

        return $order;
    }
}
