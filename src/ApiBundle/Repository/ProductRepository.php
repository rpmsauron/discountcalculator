<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Category;
use ApiBundle\Entity\Order;
use ApiBundle\Entity\Product;
use ApiBundle\Repository\RepositoryTrait;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Product Repository.
 */
class ProductRepository extends EntityRepository
{
    use RepositoryTrait;

    /**
     * Gets a Product by its id.
     *
     * @param  int $id The id to get Product for.
     *
     * @return Product The Product matching the id.
     *
     * @throws BadRequestHttpException
     */
    public function getById(int $id): Product
    {
        $product = parent::find($id);
        if (!$product instanceof Product) {
            throw new BadRequestHttpException(
                "Bad request: product does not exist."
            );
        }

        return $product;
    }

    /**
     * Returns how many of a certain Product belong to an Order.
     *
     * @param  Product $product The Product to verify.
     * @param  Order   $order   The Order to verify for.
     *
     * @return int     How many Products belong to the Order.
     */
    public function countByOrder(Product $product, Order $order): int
    {
        $queryString = "
            SELECT
              pp.quantity
            FROM
              ApiBundle:Productpack pp
            JOIN
              pp.product p
            JOIN
              pp.order o
            WHERE
              pp.product = p.id
            AND
              pp.order   = o.id
            AND
              p.id       = :productId
            AND
              o.id       = :orderId
        ";

        $query = $this->_em->createQuery($queryString);
        $query->setParameter(":productId", $product->getId());
        $query->setParameter(":orderId", $order->getId());

        $result = $query->getOneOrNullResult();

        return $result["quantity"];
    }

    /**
     * Returns different Products of a Category in an Order.
     *
     * @param  Category $category The Category to filter results by.
     * @param  Order    $order    The Order to get different Products in.
     *
     * @return Product[]  The different Products in the Order.
     */
    public function findByCategoryInOrder(Category $category, Order $order): array
    {
        $queryString = "
            SELECT
              p
            FROM
              ApiBundle:Product p,
              ApiBundle:Productpack pp,
              ApiBundle:Order o
            WHERE
              pp.product = p.id
            AND
              pp.order   = o.id
            AND
              p.category = :category
            AND
              o.id       = :orderId
        ";

        $query = $this->_em->createQuery($queryString);
        $query->setParameter(":category", $category->getId());
        $query->setParameter(":orderId", $order->getId());

        $result = $query->getResult();

        return $result;
    }

    /**
     * Finds the cheapest Product from an Order belonging to a Category.
     *
     * @param  Category $category The Category to filter by.
     * @param  Order    $order    The Order to find cheapest Product from.
     *
     * @return Product  The cheapest Product.
     */
    public function findCheapest(Category $category, Order $order): Product
    {
        $queryString = "
            SELECT
              p
            FROM
              ApiBundle:Product p,
              ApiBundle:Productpack pp,
              ApiBundle:Order o
            WHERE
              pp.order = o.id
            AND
              pp.product = p.id
            AND
              pp.product = p.id
            AND
              pp.order   = o.id
            AND
              p.category = :category
            AND
              o.id       = :orderId
            ORDER BY
              p.price
        ";

        $query = $this->_em->createQuery($queryString);
        $query->setParameter(":category", $category->getId());
        $query->setParameter(":orderId", $order->getId());
        $query->setMaxResults(1);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}
