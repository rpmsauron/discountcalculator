<?php

namespace ApiBundle\Repository;

use ApiBundle\Repository\RepositoryTrait;
use Doctrine\ORM\EntityRepository;

/**
 * Productpack Repository.
 */
class ProductpackRepository extends EntityRepository
{
    use RepositoryTrait;
}
