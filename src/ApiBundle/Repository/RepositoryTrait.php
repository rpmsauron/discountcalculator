<?php

namespace ApiBundle\Repository;

/**
 * Order Repository.
 */
trait RepositoryTrait
{
    /**
     * Removes an Entity from the database.
     *
     * @param  ApiBundle\Entity\ $entity The entity to be removed.
     *
     * @return void
     */
    public function delete($entity): void
    {
        $this->_em->remove($entity);
        $this->_em->flush($entity);
    }

    /**
     * Saves an Entity to the database.
     *
     * @param  ApiBundle\Entity\ $entity The entity to be saved.
     * @param  bool              $flush  Decides whether Entity is flushed or not.
     *
     * @return void
     */
    public function save($entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush($entity);
        }
    }

    /**
     * Saves pending Entities to database.
     *
     * @return void
     */
    public function flush(): void
    {
        $this->_em->flush();
    }
}
