<?php

namespace ApiBundle\Service;

use ApiBundle\Entity\Order;
use ApiBundle\Repository\CategoryRepository;
use ApiBundle\Repository\OrderRepository;
use ApiBundle\Repository\ProductpackRepository;
use ApiBundle\Repository\ProductRepository;

class DiscountService
{
    const TYPE_1_KEY         = "DISCOUNT_1";
    const TYPE_1_DESCRIPTION = "%s%% Discount on Order for spending over %s€.";
    const TYPE_1_MESSAGE     = "Order was %s€ and now is %s€.";

    const TYPE_2_KEY         = "DISCOUNT_2";
    const TYPE_2_DESCRIPTION = "For category '%s', you get a free product for each %s you buy.";
    const TYPE_2_MESSAGE     = "You got %s for free on Product id '%s'.";

    const TYPE_3_KEY         = "DISCOUNT_3";
    const TYPE_3_DESCRIPTION = "%s%% Discount on the cheapest product if you buy %s or more from category '%s'.";
    const TYPE_3_MESSAGE     = "Product with id '%s' went down from %s€ to %s€.";

    const DISCOUNT_LABEL     = "discount";
    const DESCRIPTION_LABEL  = "description";
    const DETAILS_LABEL      = "details";

    /**
     * @var arra
     */
    private $discounts;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ProductpackRepository
     */
    private $productpackRepository;

    /**
     * Creates a new DiscountService.
     *
     * @param CategoryRepository    $categoryRepository    The Category Repository.
     * @param OrderRepository       $orderRepository       The Order Repository.
     * @param ProductpackRepository $productpackRepository The Productpack Repository.
     * @param ProductRepository     $productRepository     The Product Repository.
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        OrderRepository $orderRepository,
        ProductpackRepository $productpackRepository,
        ProductRepository $productRepository
    ) {
        $this->categoryRepository    = $categoryRepository;
        $this->orderRepository       = $orderRepository;
        $this->productpackRepository = $productpackRepository;
        $this->productRepository     = $productRepository;
        $this->discounts             = [];
    }

    /**
     * Gets discounts that apply to a certain Order.
     *
     * @param  Order $order The Order to get discounts for.
     *
     * @return array The discounts verified or the Order.
     */
    public function getDiscounts(Order $order): array
    {
        $this->getDiscountOver1000Spent($order);
        $this->getDiscountsOnCategorySwitches($order);
        $this->getDiscountsOnCategoryTools($order);

        return $this->discounts;
    }

    /**
     * Discount 1: A customer who has already bought for over € 1000, gets a
     * discount of 10% on the whole order.
     *
     * @param  Order $order    The Order to verify if discount applies.
     *
     * @return void
     */
    private function getDiscountOver1000Spent(Order $order): void
    {
        $previousOrdersSpent = $order->getCustomer()->getTotalSpent();
        if ($previousOrdersSpent > 1000) {
            $totalWithDiscount = ($order->getTotal() * 0.9);
            $this->orderRepository->save($order);
            $this->discounts[] = [
                self::DISCOUNT_LABEL    => self::TYPE_1_KEY,
                self::DESCRIPTION_LABEL => sprintf(
                    self::TYPE_1_DESCRIPTION,
                    10,
                    1000
                ),
                self::DETAILS_LABEL     => sprintf(
                    self::TYPE_1_MESSAGE,
                    $order->getTotal(),
                    $totalWithDiscount
                )
            ];
        }
    }

    /**
     * Discount 2: For every product of category "Switches" (id 2), when you buy
     * five, you get a sixth for free.
     *
     * @param  Order $order     The Order to verify if discount applies.
     *
     * @return void
     */
    private function getDiscountsOnCategorySwitches(Order $order): void
    {
        $discountCategory     = "switches";
        $discountQuantity     = 5;

        $category             = $this->categoryRepository->findOneBy(["name" => $discountCategory]);
        $productsWithDiscount = $this->productRepository->findByCategoryInOrder($category, $order);

        foreach ($productsWithDiscount as $productWithDiscount) {
            $howMany = $this->productRepository->countByOrder($productWithDiscount, $order);
            $nrFree  = (int) ($howMany / 5); // Casts to integer to leave out division remainder.

            if ($nrFree > 0) {
                $this->discounts[] = [
                    self::DISCOUNT_LABEL    => self::TYPE_2_KEY,
                    self::DESCRIPTION_LABEL => sprintf(
                        self::TYPE_2_DESCRIPTION,
                        $discountCategory,
                        $discountQuantity
                    ),
                    self::DETAILS_LABEL     => sprintf(
                        self::TYPE_2_MESSAGE,
                        $nrFree,
                        $productWithDiscount->getId()
                    )
                ];
            }
        }
    }

    /**
     * Discount 3: If you buy two or more products of category "Tools" (id 1),
     * you get a 20% discount on the cheapest product.
     *
     * @param  Order $order The Order to verify if discount applies.
     *
     * @return void
     */
    private function getDiscountsOnCategoryTools(Order $order): void
    {
        $discountCategory     = "tools";
        $discountQuantity     = 2;

        $category = $this->categoryRepository->findOneBy(["name" => $discountCategory]);
        $productsWithDiscount = $this->productRepository->findByCategoryInOrder($category, $order);

        $totalOrderedEligibleForDiscount = 0;
        foreach ($productsWithDiscount as $productWithDiscount) {
            $howMany = $this->productRepository->countByOrder($productWithDiscount, $order);
            $totalOrderedEligibleForDiscount += $howMany;
        }

        if ($totalOrderedEligibleForDiscount >= $discountQuantity) {
            $cheapestProduct = $this->productRepository->findCheapest($category, $order);
            $discountPrice = $cheapestProduct->getPrice() * 0.8;
            $this->discounts[] = [
                self::DISCOUNT_LABEL    => self::TYPE_3_KEY,
                self::DESCRIPTION_LABEL => sprintf(
                    self::TYPE_3_DESCRIPTION,
                    20,
                    $discountQuantity,
                    $discountCategory
                ),
                self::DETAILS_LABEL     => sprintf(
                    self::TYPE_3_MESSAGE,
                    $cheapestProduct->getId(),
                    $cheapestProduct->getPrice(),
                    number_format((float) $discountPrice, 2, ".", "")
                )
            ];
        }
    }
}
